﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using B3WebBanHang.Models;
using Microsoft.EntityFrameworkCore;
namespace B3WebBanHang.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<ApplicationUser> applicationUsers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
    }

}
